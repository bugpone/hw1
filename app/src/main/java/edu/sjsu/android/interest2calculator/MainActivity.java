package edu.sjsu.android.interest2calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private EditText editText;
    private TextView resultDisplay;
    private RadioGroup radioGroup;
    private CheckBox checkBox;
    private SeekBar seekBar;
    private TextView interestDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.editText);
        resultDisplay = (TextView) findViewById(R.id.resultDisplay);
        radioGroup = (RadioGroup) findViewById(R.id.RadioGroup);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        interestDisplay = (TextView) findViewById(R.id.progressDisplay);

        interestDisplay.setText(Integer.toString(seekBar.getProgress()));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                interestDisplay.setText(Integer.toString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        resultDisplay.setText("RESULT: ");
    }


    public void onClick(View view){
        int years = getYears();
        int months = years * 12;
        boolean hasTaxes = hasTaxes();
        int interest = seekBar.getProgress();
        double payment = -1;
        double taxes = -1;
        float borrowed = getBorrowed();
        if(borrowed < 0){
            resultDisplay.setText("BAD INPUT");
            return;
        }

        if(hasTaxes){
            //0.01 is 1% while 0.001 is 0.1%
            taxes = borrowed * 0.001;
        } else {
            taxes = 0;
        }

        if(interest == 0){
            payment = (borrowed / (months)) + taxes;
        } else {
            double j = (  (double)interest /1200);
            double underJ = (1 - (   Math.pow( (1 + j),(months * -1))      ));

            double fin = (borrowed *  (j / underJ) ) + taxes;
            Log.d("MDB", "j" + j);
            Log.d("MDB", "UNDERJ:" + underJ);
            Log.d("MDB", "TAXES:" + taxes);
            payment = fin;
        }

        resultDisplay.setText("RESULT: " + String.format("%.2f",payment));
    }

    //returns -1 on error. shouldn't be negative anyways.
    public float getBorrowed(){
        String input = editText.getText().toString();
        if(input.equals("")){
            Log.d("MDB", "NO INPUT");
            return -1;
        }
        Log.d("MDB", "INPUT IS " + input);

        //got this line of code from excercise 2.
        try {
            float inputValue = Float.parseFloat(input);
            return inputValue;
        } catch (Exception e){
            Log.d("MDB", "BAD INPUT REEEEEEEEEEEE");
            e.printStackTrace();
        }
        return -1;
    }
    public int getYears(){
        int selectedId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) findViewById(selectedId);
        return Integer.parseInt(radioButton.getText().toString());
    }

    public boolean hasTaxes(){
        return checkBox.isChecked();
    }
}
